<?php

require_once ('../funzioni.php');
lugheader ('Calendario LUG', 'Eventi dei Linux Users Groups italiani');

?>

<div class="container main-contents">
	<?php include('events.html') ?>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<?php lugfooter (); ?>

